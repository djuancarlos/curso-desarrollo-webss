<!DOCTYPE html>
<html>
<head>
	<title>Formulario 2</title>
</head>
<body>

<?php
	$num1 = $_GET["numero1"];
	$num2 = $_GET["numero2"];
	$env = $_GET["enviar"];
	$res = 0;

	if ($env == "Suma") {
		$res = $num1 + $num2;
	}

	if ($env == "Resta") {
		$res = $num1 - $num2;
	}

	if ($env == "Multiplicacion") {
		$res = $num1 * $num2;
	}

	if ($env == "Divide") {
		$res = $num1 / $num2;
	}

	echo "<h3>El resultado de la $env es:".$res;
?>

</body>
</html>