<!DOCTYPE html>
<html>
<head>
	<title>Estructuiras de control en php</title>
</head>
<body>
<?php
	/*  las estructuras de control en lenguajes de programacion
		se utilizan para realizar operaciones por ejemplo: selectivas, repetitivas, y ambas;
		las estructuras de control mas utilizadas son:
		*	IF (Selectivas)
		*	FOR (repetitivas)
		*	WHILE (repetitivas-condicional)
	*/
	// Estructura de control IF, ejemplo:

	$x = 1;
	$y = 10;
	$z = 5;
	if ($x == 0) {
		echo "La variable es CERO";
	}
	else
	{
		echo "La variable no es CERO";
	}

	//estructura FOr ejemplo:
	
	$colores = $arrayName = array('azul','verde','amarillo','violeta','blanco','negro','rojo');
	$color = $arrayName = array('Gold','Grey','Pink','Blue','white','black','green','orange','purple','violet','brown','silver','red','yellow','navy blue','sky blue','ivory','chestnut','Granate','aquamarine','olive','salmon','turquoise','cooper','emerald','mauve','lavender','mustard','carmine','scarlet');
	$numeros = $arrayName = array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);

	for ($i=0; $i <= 10; $i++) { 
		echo "<br />Los nuemeros naturales hasta el 10 = " . $i;
	}

	for ($i=0; $i < 6; $i++) { 
		echo "<br />Elemento $i = [" . $colores[$i] . "]";
	}

	//funcion count(); sirve para contar los elementos de un vector
	for ($i=0; $i < count($color); $i++) { 
		echo "<br /><h2>Color: <h1 style='color:$color[$i]'>$i : $color[$i]</h1></h2>";
	}

	//ejercicio: crear un vector de 21 elementos con numeros del 1 al 20 y mostrar solo los multiplos de 2
	for ($i=0; $i < count($numeros); $i++) {
		if ($numeros[$i] % 2 == 0) {
			echo "<br />Los multiplos de 20 son = " . $numeros[$i];
		}			
	}

	for ($i=0; $i <= count($numeros); $i=$i+2) {
		echo "<br />Los multiplos de 2 son = " . $numeros[$i];
	}

?>

</body>
</html>